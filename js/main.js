(function() {
    enterView({
        // on element .row selector
        selector: ".row",
        offset: "25%",
        // when is in viewport
        enter: function(el) {
            // add class in-view
            el.classList.add("in-view");
        },
        // when is out
        exit: function(el) {
            // remove class in-view
            el.classList.remove("in-view");
        }
    });
})();