## Animation CSS in Sequence

sequence-animation.css is a simple CSS to create animation in sequence using different delays. Use [in-view.js library](https://github.com/russellgoldenberg/enter-view) to detect when element enters into view and apply a specific class named **in-view**

---

## List ToDo

1. Explain Basic Usage
2. Change README.md
3. Add examples
4. Add new CSS animation
5. Add list of classes to use

---
